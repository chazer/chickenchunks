package codechicken.chunkloader;

import net.minecraft.block.Block;
import net.minecraft.command.CommandHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import codechicken.core.CommonUtils;
import codechicken.lib.packet.PacketCustom;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

import static codechicken.chunkloader.ChickenChunks.*;

public class ChunkLoaderProxy
{
    public void init()
    {
        blockChunkLoader = new BlockChunkLoader(config.getTag("block.id").getIntValue(CommonUtils.getFreeBlockID(243)));
        blockChunkLoader.setUnlocalizedName("chickenChunkLoader").setCreativeTab(CreativeTabs.tabMisc);
        GameRegistry.registerBlock(blockChunkLoader, ItemChunkLoader.class, "chickenChunkLoader");
        
        GameRegistry.registerTileEntity(TileChunkLoader.class, "ChickenChunkLoader");
        GameRegistry.registerTileEntity(TileSpotLoader.class, "ChickenSpotLoader");
        
        PacketCustom.assignHandler(ChunkLoaderSPH.channel, 0, 255, new ChunkLoaderSPH());        
        ChunkLoaderManager.initConfig(config);
        
        MinecraftForge.EVENT_BUS.register(new ChunkLoaderEventHandler());
        TickRegistry.registerTickHandler(new ChunkLoaderEventHandler(), Side.SERVER);
        GameRegistry.registerPlayerTracker(new ChunkLoaderEventHandler());
        ChunkLoaderManager.registerMod(instance);
        
        GameRegistry.addRecipe(new ItemStack(blockChunkLoader, 1, 0), 
            " p ",
            "ggg",
            "gEg",
            'p', Item.enderPearl,
            'g', Item.ingotGold,
            'd', Item.diamond,
            'E', Block.enchantmentTable         
        );
        
        GameRegistry.addRecipe(new ItemStack(blockChunkLoader, 10, 1), 
                "ppp",
                "pcp",
                "ppp",
                'p', Item.enderPearl,
                'c', new ItemStack(blockChunkLoader, 1, 0)
        );
    }
    
    public void registerCommands(FMLServerStartingEvent event)
    {
        CommandHandler commandManager = (CommandHandler)event.getServer().getCommandManager();
        commandManager.registerCommand(new CommandChunkLoaders());
        commandManager.registerCommand(new CommandDebugInfo());
    }

    public void openGui(TileChunkLoader tile)
    {
    }
}
